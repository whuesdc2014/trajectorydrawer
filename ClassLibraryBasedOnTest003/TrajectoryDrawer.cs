﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TrajectoryDrawer {
	public partial class TrajectoryDrawerForm : Form {

		private ArrayList trajectory_cache = new ArrayList ( );
		private Point[] trajectory_displaylist;
		private Color[] trajectory_colorlist;
		private int refresh_delay = 3; 
		private int retain_count = 50;
		private Size unit_size = new Size ( 5, 5 ); 

		private void InitialTrajectoryLists ( ) {
			// Initial the trajectory lists. 
			trajectory_displaylist = new Point[retain_count];
			trajectory_colorlist = new Color[retain_count];
			int[] rainbow_assistant_array = new int[6];

			for ( int i = 0; i < trajectory_colorlist.Length; i++ ) {
				trajectory_displaylist[i] = new Point ( -1, -1 );

				rainbow_assistant_array[0]
					= rainbow_assistant_array[1]
					= 0x00;
				rainbow_assistant_array[2]
					= i % ( trajectory_colorlist.Length / 5 )
					/ ( trajectory_colorlist.Length / 5 )
					* 0xFF;
				rainbow_assistant_array[3]
					= rainbow_assistant_array[4]
					= 0xFF;
				rainbow_assistant_array[5]
					= ( trajectory_colorlist.Length / 5 - i % ( trajectory_colorlist.Length / 5 ) )
					/ ( trajectory_colorlist.Length / 5 )
					* 0xFF;

				trajectory_colorlist[i] = Color.FromArgb (
					i * 0xFF / trajectory_colorlist.Length,
					rainbow_assistant_array[( i / ( trajectory_colorlist.Length / 5 ) + 5 ) % 6],
					rainbow_assistant_array[( i / ( trajectory_colorlist.Length / 5 ) + 1 ) % 6],
					rainbow_assistant_array[( i / ( trajectory_colorlist.Length / 5 ) + 3 ) % 6] );
			}
		}

		public TrajectoryDrawerForm ( ) {

			InitialTrajectoryLists ( ); 
			InitializeComponent ( );

			Thread DrawerThread = new Thread ( DrawerThreadFunc );
			DrawerThread.IsBackground = true; 
			DrawerThread.Start ();
		}

		public void AddNewLocationPointToQueue ( int x, int y ) {
			trajectory_cache.Add ( new Point ( x, y ) );
		}

		private void DrawerThreadFunc ( ) {

			Image tempImg; 
			Graphics g; 

			while ( true ) {

				if ( this.Created == false ) {
					continue; 
				}

				// shift left
				for ( int i = 0; i < trajectory_displaylist.Length - 1; i++ ) {
					trajectory_displaylist[i] = trajectory_displaylist[i + 1];
				}
				if ( trajectory_cache.Count > 0 ) {
					trajectory_displaylist[trajectory_displaylist.Length - 1] = (Point)trajectory_cache[0];
					trajectory_cache.RemoveAt ( 0 );
				} else {
					trajectory_displaylist[trajectory_displaylist.Length - 1] = new Point ( -1, -1 );
				}
				progressBar1.Value = trajectory_displaylist.Count ( p => p.X!=-1); 

				Thread.Sleep ( refresh_delay );

				// draw
				this.Invoke ( new SetLabelsDelegate(SetLabels), trajectory_displaylist[trajectory_displaylist.Length - 1] );

				tempImg = new Bitmap ( global::TrajectoryDrawer.Properties.Resources.coordinatebgi );
				g = Graphics.FromImage ( tempImg ); 
				for ( int i = 0; i < trajectory_displaylist.Length; i++ ) {
					if ( trajectory_displaylist[i].X != -1 ) {
						g.FillRectangle ( new SolidBrush ( trajectory_colorlist[i] ), new Rectangle ( (Point)trajectory_displaylist[i], unit_size ) );
					}
				}

				this.Invoke ( new RefreshCoordinateDelegate ( RefreshCoordinate ), tempImg ); 
			}

		}

		private delegate void SetLabelsDelegate ( Point p );
		private delegate void RefreshCoordinateDelegate ( Image img );
		private delegate bool CheckValidCountOfDisplayListDelegate ( Point p); 

		private void SetLabels(Point p) {
			if ( p.X!=-1 ) {
				label1.Text = "X: " + p.X.ToString ( );
				label2.Text = "Y: " + p.Y.ToString ( );
			}
		}
		private void RefreshCoordinate ( Image img ) {
			coordinate.CreateGraphics ( ).DrawImage ( img, 0, 0 ); 
		}
		private bool CheckValidCountOfDisplayList ( Point p ) {
			return p != new Point ( -1, -1 ); 
		}

		private void trackBar1_Scroll ( object sender, EventArgs e ) {
			refresh_delay = this.trackBar1.Value; 
		}

		private void trackBar2_Scroll ( object sender, EventArgs e ) {
			retain_count = this.trackBar2.Value;
			InitialTrajectoryLists ( ); 
		}

		private void trackBar3_Scroll ( object sender, EventArgs e ) {
			unit_size = new Size ( trackBar3.Value, trackBar3.Value ); 
		}

	}
}
