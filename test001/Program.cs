﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Pipes;
using System.Threading;

namespace test001 {

	static class Program {

		/// <summary> 
		/// Directly accept the data from the pipe.
		/// </summary>
		private static string readBuffer;

		/// <summary>
		/// Temporal store the data stream from server process. 
		/// Act as a queue structure. 
		/// Data stored must be removed after use.
		/// </summary>
		private static ArrayList pipeCache = new ArrayList ( );

		/// <summary>
		/// Client stream for pipe.
		/// </summary>
		private static PipeStream pipeClient;

		/// <summary>
		/// Client pipe stream reader.
		/// </summary>
		private static StreamReader sr;

		/// <summary>
		/// The only instance of form1 in the application.
		/// </summary>
		private static Form1 Form1Instance = null;

		private static Thread pipeClientReceiver = new Thread ( pipeClientReceiverFunc );

		private static Thread pipeClientProcessor = new Thread ( pipeClientProcessorFunc );

		private static int refresh_delay;

		private static int retain_count;

		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main ( string[] args ) {

			// Must be called as a client process.
			if ( args.Length > 0 ) {

				// Initial the pipe.
				pipeClient = new AnonymousPipeClientStream ( PipeDirection.In, args[0] );
				sr = new StreamReader ( pipeClient );
				// Sync symbol && parameters.
				do {
					readBuffer = sr.ReadLine ( );
				} while ( !readBuffer.StartsWith ( "SYNC" ) );
				//MessageBox.Show ( "SYNC received" ); 
				refresh_delay = int.Parse ( readBuffer.Substring ( 4 ).Split ( new char[] { ',' } )[0] );
				retain_count = int.Parse ( readBuffer.Substring ( 4 ).Split ( new char[] { ',' } )[1] );

				//MessageBox.Show ( "refresh_delay & retain_count ready, start to create threads" ); 

				// Create the thread of pipe data receiver. 
				pipeClientReceiver.Start ( );

				// Create the thread of pipe data processor.
				pipeClientProcessor.Start ( );

				//MessageBox.Show ( "Threads ready" ); 

				// Create the form.
				Application.EnableVisualStyles ( );
				Application.SetCompatibleTextRenderingDefault ( false );
				Application.Run ( Form1Instance = new Form1(refresh_delay, retain_count) );
			} else {
				MessageBox.Show ( "Wrong way to run this application. ", "Error" );
				Application.Exit ( );
			}
		}

		/// <summary>
		/// Thread function of pipe data receiver. 
		/// Receive the pipe data stream and store into the pipeCache. 
		/// </summary>
		private static void pipeClientReceiverFunc ( ) {
			//MessageBox.Show ( "pipeClientReceiverFunc" ); 
			while ( true ) {
				if ( Form1Instance != null ) {
					if ( Form1Instance.isFormClosed == true ) {
						return;
					}
					if ( ( readBuffer = sr.ReadLine ( ) ) != null ) {
						pipeCache.Add ( readBuffer );
					}
				}
			}
		}

		/// <summary>
		/// Process the data in the pipeCache if the pipeCache is not null. 
		/// The process is only to convert the data to int and call the pipeResponder in the form instance.
		/// Remove the data when the process finished. 
		/// </summary>
		private static void pipeClientProcessorFunc ( ) {
			//MessageBox.Show ( "pipeClientProcessorFunc" ); 
			string[] coordinate_value;
			int x, y;
			while ( true ) {
				if ( Form1Instance != null ) {
					if (Form1Instance.isFormClosed == true ) {
						return;
					}
					if ( pipeCache.Count != 0 ) {
						coordinate_value = ( (string)pipeCache[0] ).Split ( new char[] { ',' } );
						x = int.Parse ( coordinate_value[0] );
						y = int.Parse ( coordinate_value[1] );
						pipeCache.RemoveAt ( 0 );
						Form1Instance.pipeResponder ( x, y );
					}
				}
			}
		}
	}
}
