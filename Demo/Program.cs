﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipes;
using System.Diagnostics; 

namespace Demo {
	class Program {
		static void Main ( string[] args ) {

			Process pipeClient = new Process ( ); 
			pipeClient.StartInfo.FileName = "E:\\Project\\ESDC2014\\TrajectoryDrawer\\test001\\bin\\Debug\\test001.exe";

			using ( AnonymousPipeServerStream pipeServer = new AnonymousPipeServerStream ( PipeDirection.Out, HandleInheritability.Inheritable ) ) {

				pipeClient.StartInfo.Arguments = pipeServer.GetClientHandleAsString ( );
				pipeClient.StartInfo.UseShellExecute = false;
				pipeClient.Start ( );

				pipeServer.DisposeLocalCopyOfClientHandle ( );

				try {
					using ( StreamWriter sw = new StreamWriter ( pipeServer ) ) {
						sw.AutoFlush = true;


						sw.WriteLine ( "SYNC30,50" );
						pipeServer.WaitForPipeDrain ( );
						System.Threading.Thread.Sleep ( 500 );

						for ( int i = 1; i < 200 && pipeClient.HasExited == false; i ++ ) {
							sw.WriteLine ( ( i + 100 ).ToString ( ) + "," + ( i + 100 ).ToString ( ) );
							sw.WriteLine ( ( i * 2 + 120 ).ToString ( ) + "," + ( i * 2 + 150 ).ToString ( ) ); 
						}

						System.Threading.Thread.Sleep ( 20 );

						for ( int i = 1; i < 200 && pipeClient.HasExited == false; i++ ) {
							sw.WriteLine ( ( i + 100 ).ToString ( ) + "," + ( i + 100 ).ToString ( ) );
						}

					}
				} catch ( IOException e ) {
					Console.WriteLine ( "[SERVER] Error: {0}", e.Message);
				}
			}
			
			pipeClient.WaitForExit ( );
			pipeClient.Close ( );
			Console.WriteLine ( "[SERVER] Client quit. Server terminating");
		}
	}
}
