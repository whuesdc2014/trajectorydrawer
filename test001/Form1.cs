﻿using System;
using System.Collections; 
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading; 

namespace test001 {
	public partial class Form1 : Form {

		private Pen imaginary = new Pen ( Color.Black, 1 );
		private Image coordinate = new Bitmap ( 500, 500 );
		private ArrayList trajectory_cache = new ArrayList ( ); 
		private Point[] trajectory_displaylist; 
		private Color[] trajectory_colorlist;
		private int refresh_delay; 
		private int retain_count;
		public bool isFormClosed = false; 

		public Form1 ( int refresh_delay, int retain_count ) {

			// Initial the imaginary pen.
			imaginary.DashStyle = System.Drawing.Drawing2D.DashStyle.Custom;
			imaginary.DashPattern = new float[] { 5, 5 };

			// Draw the image of coordinate.
			Graphics g = Graphics.FromImage ( coordinate );
			g.FillRectangle ( Brushes.White, new Rectangle ( 0, 0, 500, 500 ) );
			g.DrawLine ( imaginary, new Point ( 0, 100 ), new Point ( 500, 100 ) );
			g.DrawLine ( imaginary, new Point ( 0, 200 ), new Point ( 500, 200 ) );
			g.DrawLine ( imaginary, new Point ( 0, 300 ), new Point ( 500, 300 ) );
			g.DrawLine ( imaginary, new Point ( 0, 400 ), new Point ( 500, 400 ) );
			g.DrawLine ( imaginary, new Point ( 100, 0 ), new Point ( 100, 500 ) );
			g.DrawLine ( imaginary, new Point ( 200, 0 ), new Point ( 200, 500 ) );
			g.DrawLine ( imaginary, new Point ( 300, 0 ), new Point ( 300, 500 ) );
			g.DrawLine ( imaginary, new Point ( 400, 0 ), new Point ( 400, 500 ) );
			g.DrawImage ( coordinate, 0, 0 );

			// Initial the trajectory lists. 
			this.refresh_delay = refresh_delay; 
			this.retain_count = retain_count;
			trajectory_displaylist = new Point[retain_count];
			trajectory_colorlist = new Color[retain_count];
			for ( int i = 0; i < trajectory_colorlist.Length; i++ ) {
				trajectory_displaylist[i] = new Point ( -1, -1 ); 
				trajectory_colorlist[i] = Color.FromArgb ( i * 0xFF / trajectory_colorlist.Length, 0x00, 0x00, 0x00 );
			}

			InitializeComponent ( );

			Thread DrawerThread = new Thread ( DrawerThreadFunc );
			DrawerThread.Start ( ); 
		}

		/*
		 * if dlist[x] has no data, dlist[x] = (-1,-1)
		 * 
		 * while true: delay, dlist shift left, show
		 * 
		 * when dlist shift left, drop the first
		 * check if there is any new input
		 * if true, add it to the last location
		 * else last location = (-1, -1)
		 * 
		 * cache is required here
		 * pipeResponder receive new x & y and store them in cache as point
		 * new thread function required here
		 */

		public void pipeResponder ( int x, int y ) {
			trajectory_cache.Add ( new Point ( x, y ) ); 
		}

		private void DrawerThreadFunc() {
			//MessageBox.Show ( "pipeResponder" +x.ToString()+y.ToString()); 

			Graphics g = pictureBox1.CreateGraphics ( );

			while ( !isFormClosed ) {

				// shift left
				for ( int i = 0; i < trajectory_displaylist.Length - 1; i++ ) {
					trajectory_displaylist[i] = trajectory_displaylist[i + 1]; 
				}
				if ( trajectory_cache.Count > 0 ) {
					trajectory_displaylist[trajectory_displaylist.Length - 1] = (Point)trajectory_cache[0];
					trajectory_cache.RemoveAt ( 0 );
				} else {
					trajectory_displaylist[trajectory_displaylist.Length - 1] = new Point ( -1, -1 ); 
				}

				

				Thread.Sleep ( refresh_delay ); 

				// draw
				label1.Text = "X: " + trajectory_displaylist[trajectory_displaylist.Length - 1].X.ToString ( );
				label2.Text = "Y: " + trajectory_displaylist[trajectory_displaylist.Length - 1].Y.ToString ( ); 
				g.DrawImage ( coordinate, 0, 0 );

				for ( int i = 0; i < trajectory_displaylist.Length; i++ ) {
					if ( trajectory_displaylist[i].X != -1 ) {
						g.FillRectangle ( new SolidBrush ( trajectory_colorlist[i] ), new Rectangle ( (Point)trajectory_displaylist[i], new Size ( 5, 5 ) ) );
					}
				}


			}
			

		}

	}
}
