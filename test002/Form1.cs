﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace test002 {
	public partial class Form1 : Form {

		private ArrayList trajectory_cache = new ArrayList ( );
		private Point[] trajectory_displaylist;
		private Color[] trajectory_colorlist;
		private int refresh_delay;
		private int retain_count;

		public Form1 ( int refresh_delay, int retain_count ) {

			// Initial the trajectory lists. 
			this.refresh_delay = refresh_delay;
			this.retain_count = retain_count;
			trajectory_displaylist = new Point[retain_count];
			trajectory_colorlist = new Color[retain_count];
			int[] rainbow_assistant_array = new int[6]; 

			/*
			 * i:	imin											imax	length
			 * A:	0x00	------------------------------------>	0xFF
			 * R:	0xFF	0xFF	0x00	0x00	0x00	0xFF	0xFF
			 * G:	0x00	0x00	0x00	0xFF	0xFF	0xFF	0x00
			 * B:	0x00	0xFF	0xFF	0xFF	0x00	0x00	0x00
			 * 
			 * i:	imin	i%l		2i%l	3i%l	4i%l	5i%l	6i%l	l:=length/5
			 * A:	0x00	------------------------------------>	0xFF
			 * R:		0xFF	-i/l	0x00	0x00	+i/l	0xFF
			 * G:		0x00	0x00	+i/l	0xFF	0xFF	-i/l
			 * B:		+i/l	0xFF	0xFF	-i/l	0x00	0x00
			 *					[ ................................ ]
			 * +i/l:=(i/l*0xFF)
			 * -i/l:=((l-i)/l*0xFF)
			 */

			for ( int i = 0; i < trajectory_colorlist.Length; i++ ) {
				trajectory_displaylist[i] = new Point ( -1, -1 );

				rainbow_assistant_array[0] 
					= rainbow_assistant_array[1] 
					= 0x00;
				rainbow_assistant_array[2] 
					= i % ( trajectory_colorlist.Length / 5 ) 
					/ ( trajectory_colorlist.Length / 5 ) 
					* 0xFF;
				rainbow_assistant_array[3] 
					= rainbow_assistant_array[4] 
					= 0xFF;
				rainbow_assistant_array[5] 
					= ( trajectory_colorlist.Length / 5 - i % ( trajectory_colorlist.Length / 5 ) ) 
					/ ( trajectory_colorlist.Length / 5 ) 
					* 0xFF; 
				
				trajectory_colorlist[i] = Color.FromArgb ( 
					i * 0xFF / trajectory_colorlist.Length, 
					rainbow_assistant_array[(i / (trajectory_colorlist.Length / 5) + 5) % 6], 
					rainbow_assistant_array[(i / (trajectory_colorlist.Length / 5) + 1) % 6], 
					rainbow_assistant_array[(i / (trajectory_colorlist.Length / 5) + 3) % 6]);
			}
	
			InitializeComponent ( );

			Thread DrawerThread = new Thread ( DrawerThreadFunc );
			DrawerThread.IsBackground = true; 
			DrawerThread.Start ();
		}

		public void AddNewLocationPointToQueue ( int x, int y ) {
			trajectory_cache.Add ( new Point ( x, y ) );
		}

		private void DrawerThreadFunc ( ) {

			Image tempImg; // = new Bitmap ( global::test002.Properties.Resources.QQ截图20140507024626 );
			Graphics g; // = Graphics.FromImage ( tempImg ); 

			while ( true ) {

				// shift left
				for ( int i = 0; i < trajectory_displaylist.Length - 1; i++ ) {
					trajectory_displaylist[i] = trajectory_displaylist[i + 1];
				}
				if ( trajectory_cache.Count > 0 ) {
					trajectory_displaylist[trajectory_displaylist.Length - 1] = (Point)trajectory_cache[0];
					trajectory_cache.RemoveAt ( 0 );
				} else {
					trajectory_displaylist[trajectory_displaylist.Length - 1] = new Point ( -1, -1 );
				}

				Thread.Sleep ( refresh_delay );

				// draw
				this.Invoke ( new SetLabelsDelegate(SetLabels), trajectory_displaylist[trajectory_displaylist.Length - 1] );

				tempImg = new Bitmap ( global::test002.Properties.Resources.QQ截图20140507024626 );
				g = Graphics.FromImage ( tempImg ); 
				for ( int i = 0; i < trajectory_displaylist.Length; i++ ) {
					if ( trajectory_displaylist[i].X != -1 ) {
						g.FillRectangle ( new SolidBrush ( trajectory_colorlist[i] ), new Rectangle ( (Point)trajectory_displaylist[i], new Size ( 5, 5 ) ) );
					}
				}

				this.Invoke ( new RefreshCoordinateDelegate ( RefreshCoordinate ), tempImg ); 
			}

		}

		private delegate void SetLabelsDelegate ( Point p );
		private delegate void RefreshCoordinateDelegate ( Image img ); 

		private void SetLabels(Point p) {
			label1.Text = "X: " + p.X.ToString ( );
			label2.Text = "Y: " + p.Y.ToString ( );
		}
		private void RefreshCoordinate ( Image img ) {
			coordinate.CreateGraphics ( ).DrawImage ( img, 0, 0 ); 
		}

	}
}
