﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrajectoryDrawer; 

namespace ClassLibraryTestDemo {
	public partial class Form1 : Form {

		/// <summary>
		/// The instance of trajectory drawer form.
		/// No parameter is needed.
		/// </summary>
		TrajectoryDrawerForm tdForm = new TrajectoryDrawerForm ( );

		public Form1 ( ) {
			this.MouseMove += new System.Windows.Forms.MouseEventHandler ( this.Form1_MouseMove );
			InitializeComponent ( );
		}
	
		/// <summary>
		/// Show the trajectory drawer form instance when the main form is loading. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Form1_Load ( object sender, EventArgs e ) {
			tdForm.Show ( );
		}

		/// <summary>
		/// Add the new position to display by calling the AddNewLocationPointToQueue function.
		/// Parameter is supposed to be two integers between 0 and 500; 
		/// Do not call this function too frequent or the stack of the form MAY overflow. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Form1_MouseMove ( object sender, System.Windows.Forms.MouseEventArgs e ) {
			tdForm.AddNewLocationPointToQueue ( e.X, e.Y );
		}
	}
}
