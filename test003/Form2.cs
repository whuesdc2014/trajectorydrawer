﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test003 {
	public partial class Form2 : Form {
		private Form1 Form1Instance = new Form1 ( ); 
		public Form2 ( ) {
			this.MouseMove+=Form2_MouseMove;
			InitializeComponent ( );
		}

		private void Form2_Load ( object sender, EventArgs e ) {
			Form1Instance.Show ( ); 
		}
		void Form2_MouseMove ( object sender, System.Windows.Forms.MouseEventArgs e ) {
			Form1Instance.AddNewLocationPointToQueue ( e.X, e.Y); 
		}

	}
}
